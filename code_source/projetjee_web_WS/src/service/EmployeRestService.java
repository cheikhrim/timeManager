package service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import employe.Employe;
import metier_employe.EmployeLocal;
import presence.Presence;

@Stateless(name="BK")
@Path("/")
public class EmployeRestService {

	@EJB
	private EmployeLocal metier;

	@POST
	@Path("/employe")
	@Produces(MediaType.APPLICATION_JSON)
	public Employe addEmploye(Employe e) {
		return metier.addEmploye(e);
	}
	@GET
	@Path("/employes/{login}")
	@Produces(MediaType.APPLICATION_JSON)
	public Employe getEmploye(@PathParam(value="login") String login) {
		return metier.getEmploye(login);
	}

	@GET
	@Path("/employes")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Employe> listEmployes() {
		return metier.listEmployes();
	}
	/*
	@PUT
	@Path("/employes/delete/{e}")
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteEmploye(Employe e) {
		metier.deleteEmploye(e);
	}
	@PUT
	@Path("/employes/update/{e}")
	@Produces(MediaType.APPLICATION_JSON)
	public Employe updateEmploye( Employe e) {
		return metier.updateEmploye(e);
	}

	@GET
	@Path("/employes/{e}/presences")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Presence> listPresencesEmploye(@PathParam(value="e") Employe e) {
		return metier.listPresencesEmploye(e);
	}
	*/
}

package service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import metier_presence.PresenceLocal;
import presence.Presence;

@Stateless(name="BKP")
@Path("/")
public class PresenceRestService {
	
	@EJB
	private PresenceLocal metier;

	@POST
	@Path("/presence")
	@Produces(MediaType.APPLICATION_JSON)
	public Presence addPresence(Presence p) {
		return metier.addPresence(p);
	}
	
	@GET
	@Path("/presences/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Presence getPresence(@PathParam(value="id") String id) {
		return metier.getPresence(id);
	}
	
	@GET
	@Path("/presences")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Presence> listPresences() {
		return metier.listPresences();
	}

	/*
	@PUT
	@Path("/presences/delete/{p}")
	@Produces(MediaType.APPLICATION_JSON)
	public void deletePresence(Presence p) {
		metier.deletePresence(p);
	}

	@PUT
	@Path("/presences/update/{p}")
	@Produces(MediaType.APPLICATION_JSON)
	public Presence updatePresence(Presence p) {
		return metier.updatePresence(p);
	}
	*/
}

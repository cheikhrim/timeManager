package service;


import java.util.List;

import javax.ejb.EJB;
import javax.jws.WebMethod;
import javax.jws.WebService;

import employe.Employe;
import metier_employe.EmployeLocal;
import presence.Presence;

@WebService
public class EmployeService {
	@EJB
	private EmployeLocal metier;
	
	
	//tester avec webService m�thode liste dex pr�sences de l'employ� 
	@WebMethod
	public List<Presence> listPresencesEmploye(Employe e) {
		return metier.listPresencesEmploye(e);
	}


	@WebMethod
	public Employe addEmploye(Employe e) {
		return metier.addEmploye(e);
	}
	
}

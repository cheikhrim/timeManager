package service;

import java.util.List;

import javax.ejb.EJB;
import javax.jws.WebService;

import metier_presence.PresenceLocal;
import presence.Presence;

@WebService
public class PresenceService {
	@EJB
	private PresenceLocal metier;

	public Presence addPresence(Presence p) {
		return metier.addPresence(p);
	}

	public Presence getPresence(String id) {
		return metier.getPresence(id);
	}

	public List<Presence> listPresences() {
		return metier.listPresences();
	}

	public void deletePresence(Presence p) {
		metier.deletePresence(p);
	}

	public Presence updatePresence(Presence p) {
		return metier.updatePresence(p);
	}
	
	

}

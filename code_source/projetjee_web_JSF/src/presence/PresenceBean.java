package presence;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import employe.Employe;
import employe.EmployeBean;
import metier_presence.PresenceEJBImpl;
import metier_presence.PresenceLocal;

@ManagedBean
@SessionScoped
public class PresenceBean {
	@EJB
	private PresenceLocal metier;
	
	private Presence presence = new Presence();
	private List<Presence> presences;
	private List<Presence> presencesEmploye;
	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	public PresenceBean(){
	     metier = new PresenceEJBImpl();
	}
	
	//liste des presences
	public void list() throws IOException{
		presences = metier.listPresences();
		ec.redirect("../presence/index.xhtml");
	}
	//liste des presences d'un employ� 
	public List<Presence> listEmploye(String login){
		//recuperer l'employ� � partir de son logi
		Employe e = metier.findEmployeByLogin(login);
		presencesEmploye = metier.listPresencesEmploye(e);
		return presencesEmploye;
	}
	//ajouter un presence
	public void save(String login) throws IOException{
		//recuperer l'employ� � partir de son login
		Employe e = metier.findEmployeByLogin(login);
		presence.setEmploye(e);
		metier.addPresence(presence);
		presence = new Presence();
		
		ec.redirect("../presence/index.xhtml");
	}
	//modifier un presence
	public void update(Presence p) throws IOException{
		metier.updatePresence(p);
		ec.redirect("../presence/index.xhtml");
	}
	//supprimer un presence
	public void delete(Presence p) throws IOException{
		
		 metier.deletePresence(p);
		ec.redirect("../presence/index.xhtml");
	}
	
	public void execute() {
	        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Executed", "Pr�sence est bien ajout�."));
	    }
	
	 public void onDateSelect(SelectEvent event) {
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Date Selected", format.format(event.getObject())));
	    }
	     
	    

	public Presence getPresence() {
		return presence;
	}

	public void setPresence(Presence presence) {
		this.presence = presence;
	}

	public List<Presence> getPresences() {
		return presences;
	}

	public void setPresences(List<Presence> presences) {
		this.presences = presences;
	}

	public List<Presence> getPresencesEmploye() {
		return presencesEmploye;
	}

	public void setPresencesEmploye(List<Presence> presencesEmploye) {
		this.presencesEmploye = presencesEmploye;
	}



}

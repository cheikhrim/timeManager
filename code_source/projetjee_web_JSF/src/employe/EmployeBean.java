package employe;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import metier_employe.EmployeEJBImpl;
import metier_employe.EmployeLocal;
import presence.Presence;


@ManagedBean
@SessionScoped
public class EmployeBean {
	@EJB
	private EmployeLocal metier;
	
	private Employe employe = new Employe();
	private List<Employe> employes;
	private List<Presence> presences;
	private  List<Presence> presencesConsultation;
	
	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	
	public EmployeBean(){
	     metier = new EmployeEJBImpl();
	}
	//infos personnels de l'employe
	public String infosPerso(String login) throws IOException{
		employe = metier.getEmploye(login);
		//ec.redirect("../employe/infosPersonnels.xhtml");
		return "/employe/infosPersonnels";
	}
	//liste des employes
	public String list() throws IOException{
		employes = metier.listEmployes();
		//ec.redirect("../employe/index.xhtml");
		return "/employe/index";
	}
	//liste des employes pour consultation pr�sence
	public String listPrese() throws IOException{
			employes = metier.listEmployes();
			//ec.redirect("../employe/indexConsultation.xhtml");
			return "/employe/indexConsultation";
	}
	//ajouter un employe
	public String save() throws IOException{
		metier.addEmploye(employe);
		employe = new Employe();
		//ec.redirect("../employe/index.xhtml");
		return list();
	}
	//modifier un employe 
	public String update() throws IOException{
		metier.updateEmploye(employe);
		//ec.redirect("../employe/index.xhtml");
		return list();
	}
	//supprimer un employe
	public String delete(Employe e) throws IOException{
		metier.deleteEmploye(e);
		return list();
	}
	//liste des presences 
	public String listPresences(String login) throws IOException{
		//recuperer l'employe � partir de son login
		Employe e = metier.findEmployeByLog(login);
		presences = metier.listPresencesEmploye(e);
		return "/presence/index";
   }
	
	//liste des presences pour consultation
	public String listPresencesConsultation(Employe empl) throws IOException{
			presencesConsultation = metier.listPresencesEmploye(empl);
			return "/employe/_indexpresenceconsultation";
	   }
	public void execute() {
	        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Executed", "Employ� est bien ajout�."));
	    }

	public Employe getEmploye() {
		return employe;
	}

	public void setEmploye(Employe employe) {
		this.employe = employe;
	}

	public List<Employe> getEmployes() {
		return employes;
	}

	public void setEmployes(List<Employe> employes) {
		this.employes = employes;
	}

	public List<Presence> getPresences() {
		return presences;
	}

	public void setPresences(List<Presence> presences) {
		this.presences = presences;
	}
	public List<Presence> getPresencesConsultation() {
		return presencesConsultation;
	}
	public void setPresencesConsultation(List<Presence> presencesConsultation) {
		this.presencesConsultation = presencesConsultation;
	}
	
	
}

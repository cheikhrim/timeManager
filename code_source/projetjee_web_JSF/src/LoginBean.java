import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import metier_presence.PresenceEJBImpl;
import metier_users.UserImpl;


@ManagedBean
@SessionScoped
public class LoginBean{
	@EJB
	private UserImpl metier;
	
	private String user;
	private String pwd;
	private String statut;
	private String msgErreur;
	
	private int req;
	
	public void validateUserPassword() throws IOException{
		
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		
		if (statut.equals("Employe")) {
			boolean valide;
			try {
				valide = metier.validetEmploye(user, pwd);
				if (valide==true) {
					ec.redirect("accueil/accueilEmploye.xhtml");
				}
				else {
					msgErreur = "Login ou  mot de passe incorrect";
				    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Executed", "Erreur Authentification"));
					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*
			 finally {
			        clear();
			  }
			 */
			
		}
		else {   //admin
			boolean valide;
			try {
				valide = metier.validetAdmin(user, pwd);
				if (valide) {
					ec.redirect("accueil/accueilDRH.xhtml");
				}
				else {
					msgErreur = "Login ou mon mot de passe incorrect";
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Executed", "Erreur Authentification"));
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*
			 finally {
			        clear();
			  }
			 */
		}	
	}
	
	//deconnexion 
	public void deconnexion() throws IOException{
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		ec.redirect("../connexion.xhtml");
	}
	
	
	
	public void clear(){
		setUser(null);
		setPwd(null);
		setStatut(null);
	}

	
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public String getMsgErreur() {
		return msgErreur;
	}

	public void setMsgErreur(String msgErreur) {
		this.msgErreur = msgErreur;
	}

	public int getReq() {
		return req;
	}

	public void setReq(int req) {
		this.req = metier.getReqe();
	}

	
}

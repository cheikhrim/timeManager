package metier_employe;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import employe.Employe;
import presence.Presence;

@Stateless(name="BK")
public class EmployeEJBImpl implements EmployeLocal, EmployeRemote{
    @PersistenceContext
	private EntityManager em;
   
	@Override
	public Employe addEmploye(Employe e) {
		// TODO Auto-generated method stub
		em.persist(e);
		return e;
	}

	@Override
	public Employe getEmploye(String login) {
		Query req=em.createQuery("SELECT e FROM Employe e WHERE e.login= :login");
		req.setParameter("login", login);
	
		Employe e=(Employe)req.getSingleResult();
		
		//Employe e = em.find(Employe.class, login);
		if (e==null) throw new RuntimeException("Employe introuvable");
		return e;
	}
	@Override
	public List<Employe> listEmployes() {
		Query req=em.createQuery("SELECT e FROM Employe e");
		return req.getResultList();
	}

	@Override
	public void deleteEmploye(Employe e) {
		Employe empl= em.merge(e); //il faut d'abbord attacher l'objet � l'unit� de persistence puis faire tous les op�rations
		em.remove(empl);				
	}


	@Override
	public Employe updateEmploye(Employe e) {
		e = getEmploye(e.getLogin());
		if (e!=null) {
			em.merge(e);
		}
		else{
			addEmploye(e);
		}
		return e;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Presence> listPresencesEmploye(Employe e) {
		 Query req=em.createQuery("SELECT p FROM Presence p WHERE p.employe= :e");
		 req.setParameter("e", e);
		 
		return (List<Presence>)req.getResultList();	
	}

	@Override
	public Employe findEmployeByLog(String login) {
		Query req=em.createQuery("SELECT e FROM Employe e WHERE e.login= :login");
		req.setParameter("login", login);
	
		Employe e=(Employe)req.getSingleResult();
		
		//Employe e = em.find(Employe.class, login);
		if (e==null) throw new RuntimeException("Employe introuvable");
		return e;
	}
	
}

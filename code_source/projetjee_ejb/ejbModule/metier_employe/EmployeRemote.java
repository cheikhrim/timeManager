package metier_employe;

import java.util.List;

import javax.ejb.Remote;

import employe.Employe;
import presence.Presence;

@Remote
public interface EmployeRemote {
	//ajouter un employe
	public Employe addEmploye(Employe e);
	//consulter les infos d'un employe 
	public Employe getEmploye(String login);
	//liste des employes
	public List<Employe> listEmployes();
	//supprimer un employe
	public void deleteEmploye(Employe e);
	//modifier un employe 
	public Employe updateEmploye(Employe e);
	//recuperer la liste des pr�sence d'un employ� donn�e
	public List<Presence> listPresencesEmploye(Employe e);
	
}

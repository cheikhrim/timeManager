package employe;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for for Entity: Categorie
 * 
 */
@Entity
@Table(name="Categorie")
public class Categorie implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(nullable=false) private String description;
	
	
	public Categorie(){}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	


}

package employe;

import java.io.Serializable;


import javax.persistence.*;

/**
 * Entity implementation class for Entity: Poste
 *
 */

@Entity
@Table(name="Poste")
public class Poste implements Serializable {

	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(nullable=false) private String poste;

	

	public Poste() {
		super();
	}

	public int getId() {
		return id;
	}

	

	public void setId(int id) {
		this.id = id;
	}

	public String getPoste() {
		return poste;
	}

	public void setPoste(String poste) {
		this.poste = poste;
	}
	
   
}

package employe;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.*;

import presence.Presence;


/**
 * Entity implementation class for Entity: Employe
 *
 */
@Entity
@Table(name="Employe")
@NamedQueries({
	@NamedQuery(name="Employe.findAll", query="SELECT e FROM Employe e"),
	@NamedQuery(name="Employe.countByLoginPwd", query="SELECT count(e) FROM Employe e WHERE e.login = :login AND e.password = :password"),
	@NamedQuery(name="Employe.findEmployeByLogin", query="SELECT e FROM Employe e WHERE e.login = :login"),
})

public class Employe implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Embedded
	private Adresse adresse = new Adresse();
	
	@Column(nullable=false) private String nom;
	@Column(nullable=false) private String prenom;
	@Column(nullable=false) private String matricule;
	
	@Temporal(TemporalType.DATE)
	private Date dateNaiss;
	
	@Column(nullable=false) private String tauxOccupation;
	@Enumerated(value=EnumType.STRING) private RoleEmploye role;
	@Enumerated(value=EnumType.STRING) private RemunerationEmploye remuneration;
	@Column(nullable=false) private Double salaire;
	@Column(nullable=false) private int vacances;
	@Column(nullable=false) private Double soldeVacances;
    private int volumeLimiteAbsence;
	@Column(unique=true) private String login;
	@Column(nullable=false) private String password;
	@Column(nullable=false) private String nationnalite;
	@Temporal(TemporalType.DATE)
	private Date dateEmbauche;
	

	@ManyToOne
	@JoinColumn(name="poste")
	private Poste poste;
	
	@ManyToOne
	@JoinColumn(name="categorie")
	private Categorie categorie;

	@OneToMany(mappedBy="employe")
	private Set<Presence> presences;
	
	public Employe() {
		super();
	}
   
	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMatricule() {
		return matricule;
	}

	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}

	public Date getDateNaiss() {
		return dateNaiss;
	}

	public void setDateNaiss(Date dateNaiss) {
		this.dateNaiss = dateNaiss;
	}

	public String getTauxOccupation() {
		return tauxOccupation;
	}

	public void setTauxOccupation(String tauxOccupation) {
		this.tauxOccupation = tauxOccupation;
	}

	public RoleEmploye getRole() {
		return role;
	}

	public void setRole(RoleEmploye role) {
		this.role = role;
	}

	public RemunerationEmploye getRemuneration() {
		return remuneration;
	}

	public void setRemuneration(RemunerationEmploye remuneration) {
		this.remuneration = remuneration;
	}

	public Double getSalaire() {
		return salaire;
	}

	public void setSalaire(Double salaire) {
		this.salaire = salaire;
	}

	public int getVacances() {
		return vacances;
	}

	public void setVacances(int vacances) {
		this.vacances = vacances;
	}

	public Double getSoldeVacances() {
		return soldeVacances;
	}

	public void setSoldeVacances(Double soldeVacances) {
		this.soldeVacances = soldeVacances;
	}

	public int getVolumeLimiteAbsence() {
		return volumeLimiteAbsence;
	}

	public void setVolumeLimiteAbsence(int volumeLimiteAbsence) {
		this.volumeLimiteAbsence = volumeLimiteAbsence;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	public String getNationnalite() {
		return nationnalite;
	}

	public void setNationnalite(String nationnalite) {
		this.nationnalite = nationnalite;
	}

	public Poste getPoste() {
		return poste;
	}

	public void setPoste(Poste poste) {
		this.poste = poste;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public Set<Presence> getPresences() {
		return presences;
	}

	public void setPresences(Set<Presence> presences) {
		this.presences = presences;
	}
	

	public Date getDateEmbauche() {
		return dateEmbauche;
	}

	public void setDateEmbauche(Date dateEmbauche) {
		this.dateEmbauche = dateEmbauche;
	}
	
}

package absence;

import absence.StatutAbsence;
import employe.Employe;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Absence
 *
 */
@Entity
@Table(name="ABSENCE")
@NamedQueries({
	@NamedQuery(name="Absence.findAll", query="SELECT ab FROM Absence ab")
})
public class Absence implements Serializable {

	   
	@Id
	private int id;
	private String code;
	private Date debut;
	private Date fin;
	private String remarque;
	private StatutAbsence statut;
	private int jourOuvrable;
	private double tempsAbsence;
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name="motif")
	private Motif motif;

	@ManyToOne
	@JoinColumn(name="employe")
	private Employe employe;
	
	public Absence() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}   
	 
	public Date getDebut() {
		return debut;
	}
	public void setDebut(Date debut) {
		this.debut = debut;
	}
	public Date getFin() {
		return fin;
	}
	public void setFin(Date fin) {
		this.fin = fin;
	}
	public Employe getEmploye() {
		return employe;
	}
	public void setEmploye(Employe employe) {
		this.employe = employe;
	}
	public String getRemarque() {
		return this.remarque;
	}

	public void setRemarque(String remarque) {
		this.remarque = remarque;
	}   
	public StatutAbsence getStatut() {
		return this.statut;
	}

	public void setStatut(StatutAbsence statut) {
		this.statut = statut;
	}   
	public int getJourOuvrable() {
		return this.jourOuvrable;
	}

	public void setJourOuvrable(int jourOuvrable) {
		this.jourOuvrable = jourOuvrable;
	}   
	public double getTempsAbsence() {
		return this.tempsAbsence;
	}

	public void setTempsAbsence(double tempsAbsence) {
		this.tempsAbsence = tempsAbsence;
	}
   
}

package absence;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Motif
 *
 */
@Entity
@Table(name="MOTIF")
public class Motif implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@Column(nullable=false) private String categorie;
	private String details;
	@Column(nullable=false) private int joursAccorde;
	
	

	public Motif() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public int getJoursAccorde() {
		return joursAccorde;
	}

	public void setJoursAccorde(int joursAccorde) {
		this.joursAccorde = joursAccorde;
	}
	
	
   
}

package metier_users;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(name="BKU")
public class UserImpl {
	 @PersistenceContext
	 private EntityManager em;
	 private int reqe;
	 
	 //methode qui v�rifie si le login et le password saisies de l'employ� sont valides
	 public boolean validetEmploye(String log, String pwd) throws SQLException{
		//Query query  = em.createNamedQuery("Employe.countByLoginPwd");
		// String textReq = "SELECT e FROM Employe AS e WHERE e.login=:login and e.password=:pwd";
		 Query req=em.createQuery("SELECT COUNT(*) FROM Employe e WHERE e.login= :log and e.password= :pwd");
		 req.setParameter("log", log);
		 req.setParameter("pwd", pwd);
	
		long nb=(long)req.getSingleResult();	
	    if (nb>0){
	    		return true;
	    	}
	    else return false;
	 }
	 
	//methode qui v�rifie si le login et le password saisies de l'admin sont valides 
		 public boolean validetAdmin(String login, String pwd) throws SQLException{
			 //String textReq = ;
			 Query req=em.createQuery("SELECT COUNT(*) FROM Admin adm WHERE adm.login= :login and adm.password= :pwd");
			 req.setParameter("login", login);
			 req.setParameter("pwd", pwd);
			 long nb=(long)req.getSingleResult();	
			 if (nb>0){
			    		return true;
			    	}
			    else return false;			 
		 }

		public int getReqe() {
			return reqe;
		}

		public void setReqe(int reqe) {
			this.reqe = reqe;
		}
		 
}

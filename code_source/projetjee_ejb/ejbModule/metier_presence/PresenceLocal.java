package metier_presence;

import java.util.List;

import javax.ejb.Local;

import employe.Employe;
import presence.Presence;

@Local
public interface PresenceLocal {
	    //ajouter un presence
		public Presence addPresence(Presence p);
		//consulter les infos d'un presence 
		public Presence getPresence(String id);
		//liste des presences
		public List<Presence> listPresences();
		//supprimer un presence
		public void deletePresence(Presence p);
		//modifier un presence
		public Presence updatePresence(Presence p);
		//presence d'un employe 
		public List<Presence> listPresencesEmploye(Employe e);
		//recuperer un employe � partir de son login
		public Employe findEmployeByLogin(String login);
}

package metier_presence;

import java.util.List;

import javax.ejb.Remote;

import presence.Presence;


@Remote
public interface PresenceRemote {
	//ajouter un presence
	public Presence addPresence(Presence p);
	//consulter les infos d'un presence 
	public Presence getPresence(String id);
	//liste des presences
	public List<Presence> listPresences();
	//supprimer un presence
	public void deletePresence(Presence p);
	//modifier un presence
	public Presence updatePresence(Presence p);
	
}

package metier_presence;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import employe.Employe;
import presence.Presence;

@Stateless(name="BKP")
public class PresenceEJBImpl implements PresenceLocal, PresenceRemote{
    @PersistenceContext
	private EntityManager em;
   

	@Override
	public Presence addPresence(Presence p) {
		em.persist(p);
		return p;
	}

	@Override
	public Presence getPresence(String id) {
		Presence p=em.find(Presence.class, id);
		if (p==null) throw new RuntimeException("Presence introuvable");
		return p;
	}
 
	
	@Override
	public List<Presence> listPresences() {
		Query req=em.createQuery("SELECT p FROM Presence AS p");
		return req.getResultList();
	}

	@Override
	public List<Presence> listPresencesEmploye(Employe e) {
		Query req=em.createQuery("SELECT p FROM Presence AS p WHERE p.employe= :e");
		req.setParameter("e", e);
		return req.getResultList();
	}
	
	@Override
	public void deletePresence(Presence p) {
		Presence pr= em.merge(p); //il faut d'abbord attacher l'objet � l'unit� de persistence puis faire tous les op�rations
		em.remove(pr);		
	}

	@Override
	public Presence updatePresence(Presence p) {
		p = getPresence(p.getId());
		if (p!=null) {
			em.merge(p);
		}
		else{
			addPresence(p);
		}
		return p;
	}

	@Override
	public Employe findEmployeByLogin(String login) {
		// TODO Auto-generated method stub
		Query req=em.createQuery("SELECT e FROM Employe e WHERE e.login= :login");
		req.setParameter("login", login);
	
		Employe e=(Employe)req.getSingleResult();
		
		//Employe e = em.find(Employe.class, login);
		if (e==null) throw new RuntimeException("Employe introuvable");
		return e;
	}

	
	

}

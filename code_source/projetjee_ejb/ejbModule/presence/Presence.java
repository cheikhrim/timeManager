package presence;

import java.io.Serializable;
import java.lang.String;
import java.sql.Time;
import java.util.Date;
import javax.persistence.*;

import employe.Employe;

/**
 * Entity implementation class for Entity: Presence
 *
 */
@Entity
@Table(name="PRESENCE")
@NamedQueries({
	@NamedQuery(name="Presence.findAll", query="SELECT pres FROM Presence pres"),
	@NamedQuery(name="Presence.findPresenceByEmploye", query="SELECT p FROM Presence p WHERE p.employe= :e"),
})
public class Presence implements Serializable {

	   
	@Id
	private String id;
	private Date datePresence;
	private String heurePresence;
	private String heureRentree;
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name="employe")
	private Employe employe;

	public Presence() {
		super();
	}   
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}   
	public Date getDatePresence() {
		return this.datePresence;
	}

	public void setDatePresence(Date datePresence) {
		this.datePresence = datePresence;
	}   
	
	public String getHeurePresence() {
		return heurePresence;
	}
	public void setHeurePresence(String heurePresence) {
		this.heurePresence = heurePresence;
	}
	public String getHeureRentree() {
		return heureRentree;
	}
	public void setHeureRentree(String heureRentree) {
		this.heureRentree = heureRentree;
	}
	public Employe getEmploye() {
		return employe;
	}
	public void setEmploye(Employe employe) {
		this.employe = employe;
	}
   
}

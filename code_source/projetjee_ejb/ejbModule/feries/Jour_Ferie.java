package feries;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Jour_Ferie
 *
 */
@Entity
@Table(name="JOUR_FERIE")
public class Jour_Ferie implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@Column(nullable=false) private OccasionFerie occasion;
	@Temporal(TemporalType.DATE)
	private Date dateJF;
	
	
	public Jour_Ferie() {
		super();
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public OccasionFerie getOccasion() {
		return occasion;
	}


	public void setOccasion(OccasionFerie occasion) {
		this.occasion = occasion;
	}


	public Date getDateJF() {
		return dateJF;
	}


	public void setDateJF(Date dateJF) {
		this.dateJF = dateJF;
	}
   
}

package admin;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the admin database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Admin.findAll", query="SELECT a FROM Admin a"),
	@NamedQuery(name="Admin.countByLoginPwd", query="SELECT count(a) FROM Admin a WHERE a.login = :login AND a.password = :password"),
})
public class Admin implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String login;

	private String email;

	private String password;

	private String statut;

	public Admin() {
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStatut() {
		return this.statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

}
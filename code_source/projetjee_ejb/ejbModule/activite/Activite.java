package activite;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.*;

import employe.Employe;


/**
 * Entity implementation class for Entity: Activite
 *
 */
@Entity
@Table(name="ACTIVITE")
@NamedQueries({
	@NamedQuery(name="Activite.findAll", query="SELECT act FROM Activite act")
})
public class Activite implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String description;
	@Temporal(TemporalType.DATE)
	private Date dateActivite;
	private String remarques;
	private Time debut;
	private Time fin;
	
	@ManyToOne
	@JoinColumn(name="type_activite")
	private Type_Activite type_activite;


	@ManyToOne
	@JoinColumn(name="employe")
	private Employe employe;
	
	public Activite() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateActivite() {
		return dateActivite;
	}

	public void setDateActivite(Date dateActivite) {
		this.dateActivite = dateActivite;
	}

	public String getRemarques() {
		return remarques;
	}

	public void setRemarques(String remarques) {
		this.remarques = remarques;
	}

	public Time getDebut() {
		return debut;
	}

	public void setDebut(Time debut) {
		this.debut = debut;
	}

	public Time getFin() {
		return fin;
	}

	public void setFin(Time fin) {
		this.fin = fin;
	}

	public Type_Activite getType_activite() {
		return type_activite;
	}

	public void setType_activite(Type_Activite type_activite) {
		this.type_activite = type_activite;
	}

	public Employe getEmploye() {
		return employe;
	}

	public void setEmploye(Employe employe) {
		this.employe = employe;
	}
	
	
   
}

package activite;

public enum DescriptionActivite {
     PauseCafe, Formation, TempsEnEntreprise, TempsExterieur, PauseRepas, TravailProjet, 
     TravailGarantie, TempsVoyage;
}

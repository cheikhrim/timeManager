package activite;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;


/**
 * Entity implementation class for Entity: Type_Activite
 *
 */
@Entity
@Table(name="TYPE_ACTIVITE")
public class Type_Activite implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	private String id;
	@Column(nullable=false) private DescriptionActivite description;
	

	
	public Type_Activite() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public DescriptionActivite getDescription() {
		return description;
	}

	public void setDescription(DescriptionActivite description) {
		this.description = description;
	}

	
	
	
   
}

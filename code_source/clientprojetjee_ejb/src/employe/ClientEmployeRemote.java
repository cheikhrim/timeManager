package employe;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import metier_employe.EmployeRemote;

public class ClientEmployeRemote {

	public static void main(String[] args) {
		try {
			Context ctx = new InitialContext();
			String appName="projetjee_ear"; //nom application ear global
			String moduleName="projetjee_ejb";
			String beanName="BK"; 
			String remoteInterface=EmployeRemote.class.getName(); //nom complet de l'interface 
			String name="ejb:"+appName+"/"+moduleName+"/"+beanName+"!"+remoteInterface;
			
			EmployeRemote proxy = (EmployeRemote) ctx.lookup(name);
			//etablir la connextion et tester en ajoutant un employe 
			proxy.addEmploye(new Employe());
			
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

	}

}
